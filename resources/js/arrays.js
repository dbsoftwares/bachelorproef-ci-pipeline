function lowercaseArray(array) {
    const result = [];

    for (let i = 0; i < array.length; i++) {
        result[i] = array[i].toLowerCase();
    }

    return result;
}

function uppercaseArray(array) {
    const result = [];

    for (let i = 0; i < array.length; i++) {
        result[i] = array[i].toUpperCase();
    }

    return result;
}

module.exports = {
    uppercase: uppercaseArray, lowercase: lowercaseArray,
};
