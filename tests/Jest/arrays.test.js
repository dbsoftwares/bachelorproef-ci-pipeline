const arrays = require('../../resources/js/arrays');

const array = ["AzErTyOlPm", "JKLZ", "klmjkmljs", "QPmSDMKLll"];

test('lowercases all elements of an array', () => {
    const result = arrays.lowercase(array);

    for (let i = 0; i < array.length; i++) {
        expect(result[i]).toEqual(array[i].toLowerCase());
    }
});

test('uppercases all elements of an array', () => {
    const result = arrays.uppercase(array);

    for (let i = 0; i < array.length; i++) {
        expect(result[i]).toEqual(array[i].toUpperCase());
    }
});
